<?php

namespace Weblab\Maestro\Provider;

use Pckg\Framework\Provider;

/**
 * Class Config
 * @package Weblab\Maestro\Provider
 */
class Config extends Provider
{

    public function routes()
    {
        return [
            'url'    => [
                '/' => [
                    'view'       => 'generic',
                    'controller' => GenericController::class,
                    'name'       => 'home',
                ],
            ],
        ];
    }

}