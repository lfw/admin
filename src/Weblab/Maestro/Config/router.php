<?php

return [
    'providers' => [
        'url' => [
            '/inter'               => [
                'view'       => 'inter',
                'controller' => Weblab\Admin\Controller\Admin::class,
            ],
            '/[controller]'        => [
                'name'      => 'maestro_all',
                'view'      => 'all',
                'validate'  => [
                    'controller' => ['shop'],
                ],
                'transform' => [
                    'controller' => [
                        'prefix' => 'Weblab\\',
                    ],
                ],
            ],
            '/[controller]/[view]' => [
                'name'      => 'maestro_view_no_id',
                'validate'  => [
                    'controller' => [],
                    'view'       => ['add', 'update', 'insert'],
                ],
                'transform' => [
                    'controller' => [
                        'prefix' => '\Weblab',
                    ],
                ],
            ],
            '/[controller]/[view]' => [
                'name'      => 'maestro_sort',
                'validate'  => [
                    'controller' => [],
                    'view'       => ['sort'],
                ],
                'transform' => [
                    'controller' => [
                        'prefix' => '\Weblab',
                    ],
                ],
            ],
        ],
    ],
];