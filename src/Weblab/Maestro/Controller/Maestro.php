<?php

namespace Weblab\Maestro\Controller;

use Pckg\Database\Helper\Convention;
use Pckg\Framework\Response;
use Pckg\Framework\Router;
use Weblab\Auth\Service\Auth;
use Weblab\Logs\Record\Log;

class NewMaestro
{
    protected $config = [];

    protected $view = null;
    protected $data = [];

    public function __construct()
    {

    }

    public function __toString()
    {
        return view($this->view, $this->data);
    }
}

class Maestro
{
    protected $view = null;
    protected $data = [];
    protected $extends = 'weblab/admin/views/admin.twig';

    function __construct(Response $response)
    {
        $this->response = $response;
    }

    function setView($view)
    {
        $this->view = $view;
    }

    function buildMaestro()
    {
        $view = view($this->view);
        $view->addData($this->data);

        return $view;
    }

    // old

    function indexAction()
    {
        return view("Weblab\\Maestro\\View\\Index");
    }

    function prepareConf($conf = [], $ctrl)
    {
        $conf["url"]["sort"] = isset($conf["url"]["sort"])
            ? $conf["url"]["sort"]
            : ("/" . $ctrl . "/sort");

        $conf["url"]["add"] = isset($conf["url"]["add"])
            ? $conf["url"]["add"]
            : ("/" . $ctrl . "/add");

        $conf["url"]["edit"] = isset($conf["url"]["edit"])
            ? $conf["url"]["edit"]
            : ("/" . $ctrl . "/edit");

        $conf["url"]["view"] = isset($conf["url"]["view"])
            ? $conf["url"]["view"]
            : ("/" . $ctrl . "/view");

        $conf["url"]["delete"] = isset($conf["url"]["delete"])
            ? $conf["url"]["delete"]
            : ("/" . $ctrl . "/delete");

        $conf["rowbtn"] = isset($conf["rowbtn"])
            ? $conf["rowbtn"]
            : ["edit", "delete", "view"];

        $conf["headbtn"] = isset($conf["headbtn"])
            ? $conf["headbtn"]
            : ["add"];

        $conf['table']['class'] = isset($conf['table']['class'])
            ? $conf['table']['class']
            : "datatables";

        return $conf;
    }

    function prepareCtrl($conf)
    {
        if (!isset($conf['ctrl'])) {
            $db = debug_backtrace();
            $spacename = explode("\\", get_class($db[1]['object']));
            $ctrl = strtolower(
                $spacename[count($spacename) - 1] == $spacename[count($spacename) - 3]
                    ? $spacename[count($spacename) - 3]
                    : ($spacename[count($spacename) - 3] . "/" . $spacename[count($spacename) - 1])
            );
        } else {
            $ctrl = $conf['ctrl'];
        }

        return $ctrl;
    }

    function all($table, $conf = [])
    {
        $ctrl = $this->prepareCtrl($conf);
        $conf = $this->prepareConf($conf, $ctrl);

        return view("all", [
            "ext" => array_key_exists("extends", $conf) ? $conf["extends"] : false,
            "data" => $table,
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                ],
            ],
        ]);
    }

    function allForeign($table, $conf = [])
    {
        $ctrl = $this->prepareCtrl($conf);
        $conf = $this->prepareConf($conf, $ctrl);

        $finalTable = [];
        foreach ($table AS $i => $row) {
            $temp = [];

            if (!is_array($row)) {
                $rowArr = $row->__toArray();

                foreach ($rowArr AS $key => $val) {
                    if (substr($key, -3) == "_id" && in_array($key, array_keys($conf['heading']))) {
                        $v = $row->{"get" . Convention::toCamel(substr(ucfirst($key), 0, -3))}();
                        $rowArr[$key] = $v ? $v->__getMain() : $v;
                    }
                }
            } else {
                $rowArr = $row;
            }

            foreach ($conf["heading"] AS $hkey => $hval) {
                $temp[$hkey] = $rowArr[$hkey];
            }

            if (is_array($row) && isset($row["rowbtn"]))
                $temp["rowbtn"] = $row["rowbtn"];

            $finalTable[] = $temp;
        }

        return view("Weblab/Maestro/View/all_foreign", [
            "ext" => array_key_exists("extends", $conf) ? $conf["extends"] : false,
            "data" => $finalTable,
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                ],
            ],
        ]);
    }

    function sort($table, $conf = [])
    {
        $ctrl = end(explode("\\", Router::get("controller")));

        foreach ($table AS $i => $row) {
            $temp = [];

            foreach ($conf["heading"] AS $hkey => $hval) {
                $temp[$hkey] = $row[$hkey];
            }

            $table[$i] = $temp;
        }

        return view("sort", [
            "ext" => isset($conf["extends"]) ? $conf["extends"] : false,
            "data" => $table,
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                    "href" => "/" . $ctrl,
                ],
                [
                    "title" => __("sort"),
                ],
            ],
        ]);
    }

    function updatesort($data, $model, $pos = "position")
    {
        foreach ($data AS $position => $id) {
            $sql = "UPDATE " . $model->getTable() . " " .
                "SET " . $pos . " = " . ((int)$position + 1) . " " .
                "WHERE id = " . ((int)$id);
            DB::query($sql);
        }

        return json_encode([
            "success" => TRUE,
        ]);
    }

    function add($conf = [])
    {
        $ctrl = $this->prepareCtrl($conf);
        $conf = $this->prepareConf($conf, $ctrl);

        return view("Weblab/Maestro/View/add", [
            "ext" => isset($conf["extends"]) ? $conf["extends"] : 'vendor/lfw/admin/src/Weblab/Admin/View/admin.twig',
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                    "href" => "/" . $ctrl,
                ],
                [
                    "title" => __("add"),
                ],
            ],
        ]);
    }

    function i18nAddPrepare($i18n)
    {
        $arri18n = [];
        $lang = [1 => "Slo", 2 => "Eng", 3 => "Ita"];
        foreach ($lang AS $langId => $langTitle) {
            $obj = new $i18n;
            $obj->forceSet(["lang_id" => $langId, "_tablang" => ["id" => $langId, "title" => $langTitle]]);
            $arri18n[] = $obj;
        }
        return $arri18n;
    }

    function i18nAdd($conf)
    {
        return view("Weblab/Maestro/View/i18nadd", [
            "conf" => $conf,
        ]);
    }

    function insert($record, $plural, $redirect = TRUE, $callback = NULL)
    {
        if (!$record || ($validate = $record->validate()) && ($insert = $record->insert())) {
        }

        if (is_callable($callback))
            $callback($record);

        if (isset($_POST['ajax']))
            return json_encode([
                "success" => !!$insert,
            ]);

        if ($redirect)
            $this->response->redirect("/" . $plural . ($insert ? "/edit/" . $insert->id() : "/add"));

        return $insert;
    }

    function edit($conf = [])
    {
        $ctrl = substr(strrchr(router()->get("controller"), "\\"), 1);

        $conf["url"]["update"] = isset($conf["url"]["update"])
            ? $conf["url"]["update"]
            : ("/" . $ctrl . "/update");

        return view("Weblab/Maestro/View/edit", [
            "ext" => isset($conf["extends"]) ? $conf["extends"] : 'vendor/lfw/admin/src/Weblab/Admin/View/admin.twig',
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                    "href" => "/" . $ctrl,
                ],
                [
                    "title" => __("edit"),
                ],
            ],
        ]);
    }

    function mtm($conf = [])
    {
        $ctrl = substr(strrchr(Router::get("controller"), "\\"), 1);

        $conf["url"]["update"] = isset($conf["url"]["update"])
            ? $conf["url"]["update"]
            : ("/" . $ctrl . "/update");

        return view("Weblab/Maestro/View/mtm", [
            "ext" => isset($conf["extends"]) ? $conf["extends"] : false,
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($ctrl),
                    "href" => "/" . $ctrl,
                ],
                [
                    "title" => __("edit"),
                ],
            ],
        ]);
    }

    function i18nEditPrepare($parentEntity, $record, $entity)
    {
        $arri18n = [];
        $lang = [1 => "Slo", 2 => "Eng", 3 => "Ita"];
        foreach ($lang AS $langId => $langTitle) {
            $one = $entity->where(["lang_id" => $langId, $parentEntity->getI18nForeign() => $record->id()])->findOne();

            if ($one)
                $arri18n[] = $one->forceSet(["_tablang" => ["id" => $langId, "title" => $langTitle]]);
        }
        return $arri18n;
    }

    function i18nEdit($conf)
    {
        return view("Weblab/Maestro/View/i18nedit", [
            "conf" => $conf,
        ]);
    }

    function update($record, $plural, $redirect = TRUE, $callback = NULL)
    {
        if (!$record || !is_int($record->getID())) {
            $log = new Log(["content" => "User " . Auth::getUser()->getEmail() . " failed editing " . $plural . "#" . $record->getId() . " (invalid ID)"]);
            $log->insert();

            if ($redirect) {
                $this->response->redirect(-1);
            }
        } else if (($validate = $record->validate()) && ($update = $record->update())) {
        }

        if (is_callable($callback))
            $callback($record);

        if (isset($_POST['ajax']))
            return json_encode([
                "success" => !!$update,
            ]);

        if ($redirect)
            $this->response->redirect("/" . $plural . "/edit/" . $record->getId());

        return $update;
    }

    function delete($record, $plural, $redirect = FALSE, $callback = NULL, $precall = NULL)
    {
        if (is_callable($precall))
            $precall($record);

        if (!$record || !is_int($record->getId())) {
            $this->response->redirect(-1);
        } else if ($delete = $record->delete()) {
        }

        if (!$delete) {
        } else if (is_callable($callback))
            $callback($record);

        if ($this->getRequest()->isAjax())
            return json_encode([
                "success" => !!$delete,
            ]);

        if ($redirect)
            $this->response->redirect("/" . $plural);

        return $delete;
    }

    function view($row, $conf = [])
    {
        $data = [
            "ext" => isset($conf["extends"]) ? $conf["extends"] : false,
            "data" => $row,
            "conf" => $conf,
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
            ],
        ];

        $allControllers = [];
        $explodeController = explode("\\", Router::get("controller"));
        foreach (explode(":", end($explodeController)) AS $controller) {
            $allControllers[] = $controller;

            $data["breadcrumbs"][] = [
                "title" => Convention::toCamel($controller),
                "href" => "/" . strtolower(implode("/", $allControllers)),
            ];
        }

        $data["breadcrumbs"][] = [
            "title" => __("view"),
        ];

        return view("view", $data);
    }

    function custom($content)
    {
        return view("custom", $content);
    }

    function block($conf = [])
    {
        $ctrl = end(explode("\\", Router::get("controller")));

        $conf["url"] = isset($conf["url"])
            ? $conf["url"]
            : "#";

        return view("block", [
            "conf" => $conf,
        ]);
    }
}

?>