<?php

namespace Weblab\Admin\Controller;

use Pckg\Framework\Request;
use Pckg\Framework\Router;
use Weblab\Auth\Service\Auth;
use Weblab\Menu\Entity\Menus;
use Pckg\Manager\Asset as AssetManager;

class Admin
{

    public function interAction()
    {
        return view('inter');
    }

    function __construct(Request $request, AssetManager $assetManager, Auth $auth, Router $router)
    {
        $assetManager->execute();

        if ($router->get('name') != 'login' && $auth->isLoggedIn()) {
            //redirect(url("login"));
        }
    }

    function indexAction()
    {
        return view('Weblab\Admin:admin');
    }

    function noneAction()
    {
        return null;
    }

    // function returns layout/view // should it?
    function adminAction(Menus $menus)
    {
        $this->displaymenu = true;
        $this->adminMenu = $menus->getMenuBySlug('admin_menu');

        return view('Weblab\Admin:admin');
    }

    function withoutMenuAction(Menus $menus)
    {
        $this->adminAction($menus);

        $this->displaymenu = true;
    }
}

?>