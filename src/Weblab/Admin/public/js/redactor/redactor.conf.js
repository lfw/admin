$(document).ready(function () {
    $("textarea.form-control").redactor({
        buttonSource: true,
        replaceDivs: false,
        allowedTags: ['p', 'h1', 'h2', 'h3', 'span', 'div', 'strong', 'button', 'a', 'iframe', 'i', 'b', 'u', 'ul', 'ol', 'li', 'img', 'table', 'thead', 'tbody', 'th', 'tr', 'td'],
        imageUpload: '/redactor/upload/image', // http://imperavi.com/redactor/docs/settings/images/
        fileUpload: '/redactor/upload/image', // http://imperavi.com/redactor/docs/settings/files/
        formatting: ['p', 'h1', 'h2', 'h3', 'h4', 'h5'],
        formattingAdd: [
            {
                tag: 'span',
                title: 'Span',
                class: 'hax'
            }
        ],
        fullpage: true
    });
});