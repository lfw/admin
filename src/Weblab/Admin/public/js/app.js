var adminApp = angular.module('adminApp', []);

function lcfirst(str) {
    str += '';
    var f = str.charAt(0)
        .toLowerCase();
    return f + str.substr(1);
}
function ucfirst(str) {
    str += '';
    var f = str.charAt(0)
        .toUpperCase();
    return f + str.substr(1);
}

$.fn.serializeObject = function () {
    var data = {};
    $.each(this.serializeArray(), function (key, obj) {
        var a = obj.name.match(/(.*?)\[(.*?)\]/);
        if (a !== null) {
            var subName = new String(a[1]);
            var subKey = new String(a[2]);
            if (!data[subName]) data[subName] = {};
            if (data[subName][subKey]) {
                if ($.isArray(data[subName][subKey])) {
                    data[subName][subKey].push(obj.value);
                } else {
                    data[subName][subKey] = {};
                    data[subName][subKey].push(obj.value);
                }
                ;
            } else {
                data[subName][subKey] = obj.value;
            }
            ;
        } else {
            var keyName = new String(obj.name);
            if (data[keyName]) {
                if ($.isArray(data[keyName])) {
                    data[keyName].push(obj.value);
                } else {
                    data[keyName] = {};
                    data[keyName].push(obj.value);
                }
                ;
            } else {
                data[keyName] = obj.value;
            }
            ;
        }
        ;
    });
    return data;
};

adminApp.config(function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function (data) {
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
});

adminApp.controller('FormController', ['$scope', '$http', function ($scope, $http) {
}]);
adminApp.controller('LayoutController', ['$scope', '$http', function ($scope, $http) {

}]);
adminApp.directive('ngInitial', function ($parse) {
    return {
        restrict: "A",
        compile: function ($element, $attrs) {
            var initialValue = $attrs.value || $element.val();
            return {
                pre: function ($scope, $element, $attrs) {
                    var tagName = $element.context.tagName.toLowerCase();
                    var value = initialValue;

                    if (tagName == 'input') {
                        var inputType = $element.context.type.toLowerCase();

                        if (inputType == 'date' || inputType == 'time') {
                            value = Date.parse(value);
                        }
                    }

                    $parse($attrs.ngModel).assign($scope, value);
                }
            }
        }
    }
});
adminApp.controller('BodyController', ['$scope', '$http', function ($scope, $http) {
    $scope.handleSubmit = function (form, object) {
        if (form.$invalid) {
            return false;
        }

        var data = $('#' + form.$name).serializeObject();
        if (confirm('Post form?'))
            $http.post('', data).success(function (data) {

            });
        return true;
    };

    console.log($scope);

}]);