$(document).ready(function () {
    $("textarea.form-control").summernote({
        height: 150,
        onfocus: function (e) {
            var firstChild = parseInt($(this).children().first().position().top);
            var lastChild = parseInt($(this).children().last().position().top) + parseInt($(this).children().last().height());
            var height = firstChild + lastChild;
            if (height < 300) {
                height = 300;
            }
            $(this).animate({height: parseInt(height) + 'px'});
        },
        onblur: function (e) {
            $(this).animate({height: '150px'});
        },
        onImageUpload: function (files, editor, $editable) {
            $.each(files, function (idx, file) {
                sendFile(file, editor, $editable, file.name);
            });
        }
    });


    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);

        $.ajax({
            url: "/summernote/upload/image",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                alert(data);
                editor.insertImage(welEditable, data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + " " + errorThrown);
            }
        });
    }
});