$.fn.datetimepicker.dates['sl'] = {
    days: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "petek", "Sobota", "Nedelja"],
    daysShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob", "Ned"],
    daysMin: ["N", "P", "T", "S", "Č", "P", "S", "N"],
    months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
    today: "Danes"
};

$(document).ready(function () {

    /*$(".panel-body table.table td:last a[href^='/dynamic/tables/'].btn-warning").on("click", function(){
     $.get($(this).attr("href"), function(content){
     var content = $(content).find("#page-wrapper > .row > div").first();
     content.removeClass("col-lg-12").addClass("col-lg-10");
     var wrapper = $("#page-wrapper > .row").first();

     if (wrapper.find("> div").length > 1) {

     wrapper.find("> div").each(function(){

     });
     }
     wrapper.append(content);

     wrapper.find("> div").first().removeClass("col-lg-12").addClass("col-lg-2");

     $(".panel-heading").on("click", function(){
     var wrapper = $(this).closest("#page-wrapper > .row > div[class^='col-lg-']");
     wrapper.removeClass("col-lg-2").addClass("col-lg-10");
     wrapper.siblings().not(wrapper).removeClass("col-lg-10").addClass("col-lg-2");

     return false;
     });
     });

     return false;
     });*/

    $("a[href='#']").click(function () {
        //return false;
    });

    $(document).on("click", ".ajaxdelete", function () {
        if (confirm("Želite izbrisat zapis?")) {
            var t = this;
            $.post("/dev.php" + $(this).attr("href"), {ajax: true}, function (data) {
                if (data.success != true) {
                    alert("Napaka pri brisanju podatka.");
                } else {
                    $(t).parents("tr").slideUp(function () {
                        $(this).remove();
                    });
                }
            }, "json");
        }

        return false;
    });

    $('.dtpicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        weekStart: 1,
        autoclose: true,
        // language: "sl"
    });

    $('.dpicker').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        minView: 'month',
        //language: "sl"
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    // hide empty images
    $("img[src='']").hide();

    // add thumb preview
    $("input[type=file]").change(function (e) {
        if ($("#" + $(this).attr("id") + "_thumb").length > 0) {
            var id = $(this).attr("id");
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + id + '_thumb').attr('src', e.target.result).show();
            }

            reader.readAsDataURL(e.currentTarget.files[0]);
        }
    });

    //
    $(document).on("click", ".enable-html-editor", function () {
        $(this).parents(".form-group").find("textarea.form-control").redactor({
            minHeight: 400,
            paragraphy: false,
            convertDivs: false,
            css: ['http://getbootstrap.com/dist/css/bootstrap.css', '/src/layouts/main/public/css/css.css']
        });

        return false;
    });

    // find active menu
    $("#side-menu a").each(function () {
        if ($(this).attr("href") == document.location.pathname) {
            var parent = $(this).parent();
            parent.addClass("active");
            if (parent.parent().hasClass("nav-second-level")) {
                parent.parent().parent().addClass("active");
            } else if (parent.parent().hasClass("nav-third-level")) {
                parent.parent().parent().addClass("active");
                parent.parent().parent().parent().parent().addClass("active");
            }
        }
    });

    $('#side-menu').metisMenu();
    $('select').selectpicker();
});